import dotenv from "dotenv";
dotenv.config();

interface EnvVariables {
  PORT: string;
  // NODE_ENV: 'development' | 'production' | 'test';
  NODE_ENV: string;
  DATABASE_URL: string;
  DB_HOST: string;
  DB_PORT: string;
  DB_USER: string;
  DB_PASSWORD: string;
  DB_NAME: string;
  JWT_SECRET: string;
  SESSION_SECRET: string;
  API_KEY: string;
  API_URL: string;
  EMAIL_HOST: string;
  EMAIL_PORT: string;
  EMAIL_USER: string;
  EMAIL_PASSWORD: string;
  APP_NAME: string;
  // LOG_LEVEL: 'debug' | 'info' | 'warn' | 'error';
}
/**
 * https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/
 * https://picodotdev.github.io/blog-bitix/2021/02/introduccion-a-ddd-y-arquitectura-hexagonal-con-un-ejemplo-de-aplicacion-en-java/
 * 
 */
const CONFIG: EnvVariables = {
  PORT: process.env.PORT ?? "3000",
  NODE_ENV: process.env.NODE_ENV ?? "development",

  DATABASE_URL: process.env.DATABASE_URL ?? "mongodb://localhost:27017/mydb",
  DB_HOST: process.env.DB_HOST ?? "localhost",
  DB_PORT: process.env.DB_PORT ?? "27017",
  DB_USER: process.env.DB_USER ?? "",
  DB_PASSWORD: process.env.DB_PASSWORD ?? "",
  DB_NAME: process.env.DB_NAME ?? "mydb",

  JWT_SECRET: process.env.JWT_SECRET ?? "secret",
  SESSION_SECRET: process.env.SESSION_SECRET ?? "secret",

  API_KEY: process.env.API_KEY ?? "",
  API_URL: process.env.API_URL ?? "",

  EMAIL_HOST: process.env.EMAIL_HOST ?? "",
  EMAIL_PORT: process.env.EMAIL_PORT ?? "",
  EMAIL_USER: process.env.EMAIL_USER ?? "",
  EMAIL_PASSWORD: process.env.EMAIL_PASSWORD ?? "",

  APP_NAME: process.env.APP_NAME ?? "MyApp",
};

export default CONFIG;
