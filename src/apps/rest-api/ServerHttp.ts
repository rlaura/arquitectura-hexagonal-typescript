import express, { Router } from 'express';
import { IServer } from './IServer';
import * as http from 'http'
import routerApp from './routes/v1.routes';

export class ServerHttp implements IServer {

  private _express: express.Application;
  private _port: string;
  private _httpServer?: http.Server;

  constructor(port: string) {
    this._port = port;
    this._express = express();

    // const router = Router();
    this._express.use(routerApp);
  }

  async listen(): Promise<void> {
    return new Promise((resolve) => {
      /**
       * le asignamos a this._httpServer la parte de _express para que luego se pueda hacer un
       * this._httpServer.close para detener express, esto podra no parecer importante ahora,
       * pero para cuando se quiera configurar para los entornos de ejecucion como developer o production
       * servira bastante
       */
      this._httpServer = this._express.listen(this._port, () => {
        console.log(
          `Servidor http corriendo en http://localhost:${this._port}`
        )
        console.log('  Presione CTRL-C para detener\n')
        resolve()
      })
      resolve();
    });
  }

  async stop (): Promise<void> {
    return await new Promise((resolve, reject) => {
      if (this._httpServer != null) {
        this._httpServer.close(error => {
          if (error != null) {
            return reject(error)
          }
          return resolve()
        })
      }

      return resolve()
    })
  }

}