// import { Router } from 'express';
// import glob from 'glob';


/**
 * https://github.com/kgrzybek/modular-monolith-with-ddd/blob/master/src/API/CompanyName.MyMeetings.API/Modules/UserAccess/UserRegistrationsController.cs
 * https://github.com/ivanpaulovich/clean-architecture-manga/blob/main/accounts-api/src/WebApi/UseCases/V1/Accounts/GetAccount/AccountsController.cs
 * https://github.com/kgrzybek/sample-dotnet-core-cqrs-api/blob/master/src/SampleProject.API/Customers/CustomersController.cs
 * https://github.com/mehdihadeli/food-delivery-microservices/blob/main/src/BuildingBlocks/BuildingBlocks.Security/ApiKey/Authorization/Roles.cs
 * https://github.com/henriquelourente/Domain-Driven-Design-Sample/blob/master/src/SampleLibrary.Api/Controllers/BookController.cs
 * https://github.com/amolines/cqrs/blob/develop/demo/CitiBank.Api/Controllers/AccountsController.cs
 * 
 */