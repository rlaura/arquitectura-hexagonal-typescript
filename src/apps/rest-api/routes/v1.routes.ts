import express from 'express';
import UsersRoutes from '../../../modules/users/UserRegistrations/interfaces/routes/UsersRoutes.routes';
const routerApp = express.Router();

routerApp.use("/api/v1",UsersRoutes);

export default routerApp;