export interface IServer {
  listen() : Promise<void>;
  stop() : Promise<void>;
}