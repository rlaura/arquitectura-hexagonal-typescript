import express, { Router } from "express";
import { IServer } from "./IServer";
import * as https from "https";

export class ServerHttps implements IServer {
  private _express: express.Application;
  private _port: string;
  private _httpsServer?: https.Server;
  private _options: https.ServerOptions;

  constructor(port: string, options: https.ServerOptions) {
    this._port = port;
    this._express = express();
    this._options = options;

    const router = Router();
    this._express.use(router);
  }

  async listen(): Promise<void> {
    return new Promise((resolve) => {
      this._httpsServer = https
        .createServer(this._options, this._express)
        .listen(this._port, () => {
          console.log(
            `Servidor https corriendo en el puerto ${this._port}`
          );
          console.log("  Presione CTRL-C para detener\n");
          resolve();
        });
    });
  }

  async stop(): Promise<void> {
    return await new Promise((resolve, reject) => {
      if (this._httpsServer != null) {
        this._httpsServer.close((error) => {
          if (error != null) {
            return reject(error);
          }
          return resolve();
        });
      }

      return resolve();
    });
  }
}
