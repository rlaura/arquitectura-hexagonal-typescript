import { DomainEventSubscribers } from "../../../infrastructure/bus/event/DomainEventSubscribers";
import { DomainEvent } from "./DomainEvent";

/**
 * Event Bus (Bus de Eventos)
 * Función: Se utiliza para manejar eventos que representan cambios en el estado del sistema,
 * Ejemplos de consultas: Un usuario crea una nueva cuenta (evento). El bus de eventos publica un mensaje "UsuarioCreadoEvento".
 *                        Componentes como un servicio de notificaciones o un servicio de registro podrían estar suscritos a este evento.
 * Momento de uso: Los eventos representan hechos que ya han ocurrido en el sistema, como la creación de un nuevo usuario,
 *                 la actualización de un perfil, la publicación de un mensaje, etc.
 */

/**
 * define un contrato para un bus de eventos en un sistema
 */
export interface IEventBus {
  /**
   * Publica eventos en el bus de eventos.
   * @param {DomainEvent[]} events - Los eventos a publicar.
   * @return {Promise<void>} Una promesa que se resuelve una vez que los eventos se han publicado correctamente.
   */
  publish(events: DomainEvent[]): Promise<void>;

  /**
   * Agrega suscriptores al bus de eventos.
   * @param {DomainEventSubscribers} subscribers - Los suscriptores a agregar.
   */
  addSubscribers(subscribers: DomainEventSubscribers): void;
}
