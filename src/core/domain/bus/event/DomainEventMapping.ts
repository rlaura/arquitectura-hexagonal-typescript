import { DomainEvent, DomainEventClass } from "./DomainEvent";
import { IDomainEventSubscriber } from "./IDomainEventSubscriber";
/**
 * Define un alias de tipo Mapping que representa un mapa donde las claves
 * son cadenas y los valores son clases de eventos de dominio (DomainEventClass).
 */
type Mapping = Map<string, DomainEventClass>;
/**
 * se utiliza para mapear nombres de eventos de dominio a sus clases correspondientes.
 */
export class DomainEventMapping {
  /**
   * propiedad privada que es un mapa que mapea nombres de eventos a sus clases correspondientes.
   */
  private mapping: Mapping;
  /**
   * toma una lista de suscriptores de eventos de dominio y crea el mapeo correspondiente.
   * @param mapping Un array que contiene los suscriptores de eventos de dominio que se van a agregar al bus de eventos.
   *                Cada suscriptor debe implementar la interfaz IDomainEventSubscriber con el tipo genérico DomainEvent
   */
  constructor(mapping: IDomainEventSubscriber<DomainEvent>[]) {
    /**
     * Utiliza reduce para iterar sobre la lista de suscriptores y crear el mapa mapping.
     * Utiliza la función eventNameExtractor() para agregar los nombres de los eventos
     * y sus clases al mapa mapping.
     */
    this.mapping = mapping.reduce(
      this.eventsExtractor(),
      new Map<string, DomainEventClass>()
    );
  }
  /**
   * devuelve una función que se utiliza para extraer los nombres de los eventos
   * y sus clases correspondientes de un suscriptor de eventos
   * @returns Retorna una función que toma un mapa y un suscriptor de evento y agrega los eventos al mapa.
   */
  private eventsExtractor() {
    // Retorna una función que toma un mapa y un suscriptor de evento y agrega los eventos al mapa.
    return (map: Mapping, subscriber: IDomainEventSubscriber<DomainEvent>) => {
      // Itera sobre los eventos suscritos por el suscriptor y agrega cada evento al mapa.
      subscriber.subscribedTo().forEach(this.eventNameExtractor(map));
      return map;
    };
  }
  /**
   * Método privado que devuelve una función que se utiliza para agregar un nombre de evento
   * y su clase correspondiente al mapa mapping.
   * @param map representa un mapa donde las claves son cadenas y los valores son clases de eventos de dominio
   * @returns Retorna una función que toma una clase de evento de dominio y la agrega al mapa.
   */
  private eventNameExtractor(
    map: Mapping
  ): (domainEvent: DomainEventClass) => void {
    // Retorna una función que toma una clase de evento de dominio y la agrega al mapa.
    return (domainEvent) => {
      // Obtiene el nombre del evento y lo utiliza como clave para agregar la clase de evento al mapa.
      const eventName = domainEvent.EVENT_NAME;
      map.set(eventName, domainEvent);
    };
  }
  /**
   * Método que busca en el mapa mapping una clase de evento de dominio correspondiente al nombre proporcionado y la devuelve.
   * Si no se encuentra ninguna clase para el nombre dado, devuelve undefined.
   * @param name nombre
   * @returns una clase de evento de dominio correspondiente al nombre proporcionado
   */
  for(name: string) {
    // Obtiene la clase de evento de dominio del mapa mapping usando el nombre proporcionado como clave.
    const domainEvent = this.mapping.get(name);
    // Comprueba si la clase de evento de dominio obtenida es undefined (es decir, si no se encontró ninguna clase para el nombre dado).
    // Si es así, el método devuelve inmediatamente.
    if (!domainEvent) {
      return;
    }

    // Si se encontró una clase de evento de dominio correspondiente al nombre proporcionado, el método la devuelve.
    return domainEvent;
  }
}
