import { DomainEvent, DomainEventClass } from "./DomainEvent";
/**
 * interface que describe el contrato para un suscriptor de eventos de dominio
 * proporciona un contrato para las clases que desean actuar como suscriptores de eventos de dominio.
 * @param <T extends DomainEvent> T es un tipo genérico que debe ser o extender de la clase DomainEvent
 */
export interface IDomainEventSubscriber<T extends DomainEvent> {
  /**
   * Devuelve la lista de clases de eventos de dominio a las que este suscriptor está suscrito.
   * @return {DomainEventClass[]} La lista de clases de eventos de dominio.
   */
  subscribedTo(): Array<DomainEventClass>;
  /**
   * Este método define la acción que se realiza cuando el suscriptor recibe un evento de dominio.
   * devuelve una promesa que se resuelve cuando el suscriptor maneja el evento
   * Maneja un evento de dominio.
   * @param {T} domainEvent - El evento de dominio a manejar.
   * @return {Promise<void>} Una promesa que se resuelve una vez que el evento ha sido manejado correctamente.
   */
  on(domainEvent: T): Promise<void>;
}
