import { Uuid } from "../../value-objects/Uuid";

/**
 * se utiliza para mapear nombres de eventos de dominio a sus clases correspondientes.
 * Esta clase es abstracta, lo que significa que no se puede instanciar directamente,
 * sino que debe ser extendida por clases concretas.
 */
export abstract class DomainEvent {
  /**
   * El nombre del evento.
   */
  static EVENT_NAME: string;
  /**
   * El ID del agregado asociado al evento.
   */
  readonly aggregateId: string;
  /**
   * El ID del evento.
   */
  readonly eventId: string;
  /**
   * La fecha y hora en que ocurrió el evento.
   */
  readonly occurredOn: Date;
  /**
   * El nombre del evento.
   */
  readonly eventName: string;

  static fromPrimitives: (params: {
    aggregateId: string;
    eventId: string;
    occurredOn: Date;
    attributes: DomainEventAttributes;
  }) => DomainEvent;

  /**
   * Crea una nueva instancia de DomainEvent.
   * @param {string} eventName - El nombre del evento.
   * @param {string} aggregateId - El ID del agregado asociado al evento.
   * @param {string} [eventId] - El ID del evento (opcional, se genera uno aleatorio si no se proporciona).
   * @param {Date} [occurredOn] - La fecha y hora en que ocurrió el evento (opcional, se usa la fecha y hora actual si no se proporciona).
   * @return {DomainEvent} Una nueva instancia de DomainEvent.
   */
  constructor(params: {
    eventName: string;
    aggregateId: string;
    eventId?: string;
    occurredOn?: Date;
  }) {
    const { aggregateId, eventName, eventId, occurredOn } = params;
    this.aggregateId = aggregateId;
    this.eventId = eventId || Uuid.generate().value;
    this.occurredOn = occurredOn || new Date();
    this.eventName = eventName;
  }

  /**
   * Convierte el evento de dominio a su representación primitiva.
   * @return {DomainEventAttributes} La representación primitiva del evento de dominio.
   */
  abstract toPrimitives(): DomainEventAttributes;
}

export type DomainEventClass = {
  /**
   * El nombre del evento.
   */
  EVENT_NAME: string;
  /**
   * Convierte los parámetros primitivos en una instancia del evento de dominio.
   * @param {string} aggregateId - El ID del agregado asociado al evento.
   * @param {string} eventId - El ID del evento.
   * @param {Date} occurredOn - La fecha y hora en que ocurrió el evento.
   * @param {DomainEventAttributes} attributes - Los atributos del evento.
   * @return {DomainEvent} Una nueva instancia del evento de dominio.
   */
  fromPrimitives(params: {
    aggregateId: string;
    eventId: string;
    occurredOn: Date;
    attributes: DomainEventAttributes;
  }): DomainEvent;
};

/**
 * Define un tipo para los atributos de un evento de dominio.
 */
type DomainEventAttributes = any;
