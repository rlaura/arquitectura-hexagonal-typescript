import { IQuery } from "./IQuery";
import { IResponse } from "./IResponse";

/**
 * @ Q extends IQuery    --> Q debe ser una subclase o implementación de la interfaz IQuery
 * @ R extends IResponse --> R debe ser una subclase o implementación de la interfaz IResponse
 */
export interface IQueryHandler<Q extends IQuery, R extends IResponse> {
  /**
   * devuelve un objeto de tipo Query
   */
  subscribedTo(): IQuery;
  /**
   * Devuelve una promesa que eventualmente se resuelve en un tipo R, que es una subclase de IResponse,
   * el resultado será del tipo específico que extiende 
   * @param query objeto de tipo Q (que es una subclase de IQuery) y lo maneja
   */
  handle(query: Q): Promise<R>;
}
