import { IQuery } from './IQuery';
/**
 * QueryNotRegisteredError es una clase que se utiliza para
 * llamar al constructor de la clase base Error con un mensaje de error personalizado
 */
export class QueryNotRegisteredError extends Error {
  /**
   * Llama al constructor de la clase base Error con un mensaje de error personalizado
   * @param query es un objeto tipo IQuery.
   */
  constructor(query: IQuery) {
    /**
     * Llama al constructor de la clase base Error con un mensaje de error personalizado
     */
    super(`La consulta <${query.constructor.name}> no tiene un controlador de consultas asociado`);
    // super(`The query <${query.constructor.name}> hasn't a query handler associated`);
  }
}
