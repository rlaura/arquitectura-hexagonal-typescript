/**
 * proporciona un conjunto de métodos para registrar diferentes tipos de mensajes, 
 * como mensajes de depuración, errores e información
 */
export default interface ILogger {
  
  /**
   * Este método toma un mensaje de tipo string
   * @param message mensaje de depuración. 
   */
  debug(message: string): void;

  /**
   * Este método toma un mensaje de tipo string
   * @param message mensaje de error.
   */
  error(message: string | Error): void;

  /**
   * Este método toma un mensaje de tipo string
   * @param message mensaje informativo.
   */
  info(message: string): void;
}
