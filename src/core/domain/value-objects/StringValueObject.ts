/**
 * proporciona una estructura básica para la implementación de objetos de valor de tipo cadena
 * se utiliza esta estructura para representar y manipular valores de cadena
 */
// export abstract class StringValueObject {
//   /**
//    * propiedad de solo lectura que almacena el valor de cadena del objeto de valor.
//    */
//   readonly value: string;

//   /**
//    * representa el valor de cadena del objeto de valor.
//    * @param value
//    */
//   constructor(value: string) {
//     this.value = value;
//   }

//   /**
//    * Método que devuelve una representación de cadena del objeto de valor.
//    * @returns el valor de cadena almacenado en la propiedad value.
//    */
//   toString(): string {
//     return this.value;
//   }
// }
import { ValueObject } from "./ValueObject";
export abstract class StringValueObject extends ValueObject<string> {}
