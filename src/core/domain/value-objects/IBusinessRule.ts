/**
 * Define una regla de negocio.
 */
export interface IBusinessRule {
  /**
   * Verifica si la regla de negocio está rota.
   * @return {boolean} Devuelve true si la regla está rota, de lo contrario devuelve false.
   */
  isBroken(): boolean;

  /**
   * El mensaje descriptivo de la regla de negocio.
   * @return {string} El mensaje descriptivo de la regla de negocio.
   */
  message: string;
}
