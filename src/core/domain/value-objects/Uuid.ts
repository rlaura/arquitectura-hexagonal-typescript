import { v4 as uuid } from 'uuid';
import validate from 'uuid-validate';
import { ValueObject } from './ValueObject';
import { InvalidArgumentError } from '../exceptions/InvalidArgumentError';
/**
 * Representa un UUID (Identificador Único Universal) como un objeto de valor.
 */
export class Uuid extends ValueObject<string>{
  /**
   * Crea una nueva instancia de Uuid.
   * @param {string} value - El valor del UUID.
   * @return {Uuid} Una nueva instancia de Uuid.
   * @throws {InvalidArgumentError} Se lanza si el UUID no es válido.
   */
  constructor(value: string) {
      super(value);
      this.ensureIsValidUuid(value);
  }

  /**
   * Genera un UUID aleatorio.
   * @return {Uuid} Un nuevo UUID generado aleatoriamente.
   */
  static generate(): Uuid {
      return new Uuid(uuid());
  }

  /**
   * Verifica si un UUID es válido.
   * @param {string} id - El UUID a verificar.
   * @throws {InvalidArgumentError} Se lanza si el UUID no es válido.
   */
  private ensureIsValidUuid(id: string): void {
      if (!validate(id)) {
          throw new InvalidArgumentError(`<${this.constructor.name}> no permite el valor <${id}>`);
      }
  }
}