/**
 * define un tipo de dato genérico llamado Nullable<T>, 
 * @ puede ser un valor de tipo T o puede ser null, lo que significa que no hay ningún valor.
 * 
 * @ Nullable<T>: Es un tipo de dato genérico que toma un tipo T como parámetro.
 * @ T | null: Esto indica que el tipo Nullable<T> puede contener un valor de tipo T o null
 */
export type Nullable<T> = T | null;
