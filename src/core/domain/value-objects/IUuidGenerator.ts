/**
 * describe un contrato para un generador de identificadores únicos universales (UUID)
 */
export interface IUuidGenerator {
  /**
   * metodo que devuelve una cadena que representa un UUID generado.
   */
  generate(): string;
}