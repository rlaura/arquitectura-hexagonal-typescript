/**
 * proporciona una estructura básica para la implementación de objetos de valor enumerados
 * Estos objetos de valor tienen un valor único que debe estar dentro de una lista de valores válidos
 * se espera que las clases que heredan de esta clase manejen los errores cuando se proporciona un valor no válido
 */
export abstract class EnumValueObject<T> {
  /**
   * propiedad de solo lectura que tiene un tipo genérico T
   */
  readonly value: T;
  /**
   * 
   * @param value generico T que representa el valor del objeto de valor enumerado
   * @param validValues es una matriz de valores válidos para el objeto de valor enumerado
   */
  constructor(value: T, public readonly validValues: T[]) {
    this.value = value;
    this.checkValueIsValid(value);
  }

  /**
   * Comprueba si el valor está incluido en la matriz de validValues es valido. 
   * Si el valor no es válido, se invoca el método throwErrorForInvalidValue
   * @param value generico T que representa el valor del objeto de valor enumerado
   */
  public checkValueIsValid(value: T): void {
    if (!this.validValues.includes(value)) {
       // Si el valor no es válido, se lanza un error.
      this.throwErrorForInvalidValue(value);
    }
  }
  /**
   * Se espera que este método maneje el lanzamiento de un error cuando se detecta que el valor no es válido.
   * @param value enerico T que representa el valor del objeto de valor enumerado
   */
  protected abstract throwErrorForInvalidValue(value: T): void;
}
