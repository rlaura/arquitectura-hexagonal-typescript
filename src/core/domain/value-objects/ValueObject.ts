import { InvalidArgumentError } from "../exceptions/InvalidArgumentError";

/**
 * Define los tipos primitivos aceptados.
 */
export type Primitives = string | number | boolean | Date;

/**
 * Clase abstracta que representa un objeto de valor.
 * proporciona una estructura básica para la implementación de objetos de valor genéricos
 * Estos objetos de valor encapsulan un valor y proporcionan métodos para comparar,
 * serializar y obtener representaciones de cadena de dicho valor.
 */
export abstract class ValueObject<T extends Primitives> {
  /**
   * El valor del objeto de valor.
   */
  readonly value: T;

  /**
   * Crea una nueva instancia de ValueObject.
   * @param {T} value - El valor del objeto de valor.
   * @return {ValueObject<T>} Una nueva instancia de ValueObject.
   * @throws {InvalidArgumentError} Se lanza si el valor no está definido.
   */
  constructor(value: T) {
    this.value = value;
    this.ensureValueIsDefined(value);
  }

  /**
   * Verifica si este objeto de valor es igual a otro objeto de valor.
   * @param {ValueObject<T>} other - El otro objeto de valor a comparar.
   * @return {boolean} Devuelve true si los objetos de valor son iguales, de lo contrario devuelve false.
   */
  equals(other: ValueObject<T>): boolean {
    return (
      other.constructor.name === this.constructor.name &&
      other.value === this.value
    );
  }

  /**
   * Devuelve una representación de cadena del objeto de valor.
   * @return {string} Una representación de cadena del objeto de valor.
   */
  toString(): string {
    return this.value.toString();
  }

  /**
   * Verifica si el valor está definido.
   * @param {T} value - El valor a verificar.
   * @throws {InvalidArgumentError} Se lanza si el valor no está definido.
   */
  private ensureValueIsDefined(value: T): void {
    if (value === null || value === undefined) {
      throw new InvalidArgumentError(
        "El valor del Value Object debe ser definido."
      );
      // throw new InvalidArgumentError("Value must be defined");
    }
  }
}
