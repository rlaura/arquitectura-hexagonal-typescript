/**
 * proporciona métodos para comparar y realizar operaciones con objetos de valor numéricos
 */
// export abstract class NumberValueObject {
//   /**
//    * propiedad de solo lectura que almacena el valor numérico del objeto de valor.
//    */
//   readonly value: number;
//   /**
//    * toma un parámetro value, que representa el valor numérico del objeto de valor.
//    * @param value es un valor numerico
//    */
//   constructor(value: number) {
//     this.value = value;
//   }
//   /**
//    * Método que compara el valor del objeto de valor actual con el valor de otro objeto de valor numérico (other).
//    * Devuelve true si los valores son iguales y false en caso contrario.
//    * @param other valor de tipo NumberValueObject
//    * @returns true si los valores son iguales y false en caso contrario.
//    */
//   equalsTo(other: NumberValueObject): boolean {
//     return this.value === other.value;
//   }
//   /**
//    *
//    * @param other
//    * @returns
//    */
//   isBiggerThan(other: NumberValueObject): boolean {
//     return this.value > other.value;
//   }
// }

import { ValueObject } from "./ValueObject";

/**
 * Clase abstracta que representa un objeto de valor numérico.
 */
export abstract class NumberValueObject extends ValueObject<number> {
  /**
   * Verifica si este objeto de valor numérico es mayor que otro objeto de valor numérico.
   * @param {NumberValueObject} other - El otro objeto de valor numérico a comparar.
   * @return {boolean} Devuelve true si este objeto de valor es mayor que el otro, de lo contrario devuelve false.
   */
  isBiggerThan(other: NumberValueObject): boolean {
    return this.value > other.value;
  }
}
