import { DomainEvent } from "../bus/event/DomainEvent";
import { BusinessRuleValidationException } from "../exceptions/BusinessRuleValidationException";
import { IBusinessRule } from "../value-objects/IBusinessRule";

/**
 * clase abstracta que representa la raíz de un agregado en un modelo de dominio.
 */
export abstract class AggregateRoot {
  /**
   * es un array que almacenará los eventos de dominio registrados por el agregado.
   */
  private domainEvents: Array<DomainEvent>;
  /**
   * Constructor que inicializa la propiedad domainEvents como un
   * array vacío cuando se crea una nueva instancia de la clase.
   */
  constructor() {
    this.domainEvents = [];
  }
  /**
   * Método que extrae y devuelve todos los eventos de dominio registrados por el agregado hasta el momento.
   * Luego, vacía el array domainEvents para que no se dupliquen los eventos.
   * @returns domainEvents: devuelve todos los eventos de dominio registrados por el agregado hasta el momento.
   */
  pullDomainEvents(): Array<DomainEvent> {
    // Copia los eventos de dominio
    const domainEvents = this.domainEvents.slice();
    // Vacía el array domainEvents
    this.domainEvents = [];
    // Devuelve los eventos de dominio
    return domainEvents;
  }
  /**
   * Método que registra un nuevo evento de dominio al agregarlo al array domainEvents.
   * @param event El evento de dominio que se va a registrar. Este parámetro representa
   *              un evento específico que ha ocurrido en el contexto del agregado y
   *              que se desea registrar para su posterior procesamiento o almacenamiento.
   */
  addDomainEvent(event: DomainEvent): void {
    this.domainEvents.push(event);
  }
  /**
   * Método abstracto que debe ser implementado por las clases concretas que heredan de AggregateRoot.
   * Debe devolver una representación primitiva del agregado.
   */
  abstract toPrimitives(): any;
  
  /**
   * Verifica si la regla de negocio está rota y, si es así, 
   * lanza una excepción de validación de regla de negocio.
   * @param rule 
   */
  protected checkRule(rule: IBusinessRule): void {
    if (rule.isBroken()) {
      throw new BusinessRuleValidationException(rule);
    }
  }
}
