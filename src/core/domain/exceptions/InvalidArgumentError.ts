/**
 * se utiliza para representar errores relacionados con argumentos inválidos en una aplicación.
 * Puedes usar esta clase para lanzar excepciones cuando encuentres argumentos que 
 * no cumplen con ciertas condiciones en tu código.
 */
export class InvalidArgumentError extends Error {}
