import { IBusinessRule } from "../value-objects/IBusinessRule";
/**
 * Excepción lanzada cuando una regla de negocio se invalida.
 */
export class BusinessRuleValidationException extends Error {
  /**
   * La regla de negocio que se ha roto.
   * @param {IBusinessRule} brokenRule - La regla de negocio que se ha roto.
   */
  public BrokenRule: IBusinessRule;

  /**
   * Detalles adicionales sobre la excepción.
   * @param {string} Details - Detalles adicionales sobre la excepción.
   */
  public Details: string;

  /**
   * Crea una instancia de BusinessRuleValidationException.
   * @param {IBusinessRule} brokenRule - La regla de negocio que se ha roto.
   * @return {BusinessRuleValidationException} Una nueva instancia de BusinessRuleValidationException.
   */
  constructor(brokenRule: IBusinessRule) {
    super(brokenRule.message);
    this.BrokenRule = brokenRule;
    this.Details = brokenRule.message;
  }

  /**
   * Obtiene una representación de cadena de la excepción.
   * @return {string} Una representación de cadena de la excepción.
   */
  public toString(): string {
    return `${this.BrokenRule.constructor.name}: ${this.BrokenRule.message}`;
  }
}
