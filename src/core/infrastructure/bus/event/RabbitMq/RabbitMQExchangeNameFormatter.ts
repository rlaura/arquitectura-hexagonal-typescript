/**
 * Clase para formatear nombres de intercambio RabbitMQ.
 */
export class RabbitMQExchangeNameFormatter {
  /**
   * Formatea el nombre de intercambio para el intercambio de reintento.
   * @param {string} exchangeName - El nombre del intercambio original.
   * @return {string} El nombre del intercambio de reintento formateado.
   */
  public static retry(exchangeName: string): string {
    return `retry-${exchangeName}`;
  }
  
  /**
   * Formatea el nombre de intercambio para el intercambio de carta muerta.
   * @param {string} exchangeName - El nombre del intercambio original.
   * @return {string} El nombre del intercambio de carta muerta formateado.
   */
  public static deadLetter(exchangeName: string): string {
    return `dead_letter-${exchangeName}`;
  }
}
