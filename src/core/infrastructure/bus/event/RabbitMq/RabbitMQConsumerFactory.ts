import { DomainEvent } from "../../../../domain/bus/event/DomainEvent";
import { IDomainEventSubscriber } from "../../../../domain/bus/event/IDomainEventSubscriber";
import { DomainEventDeserializer } from "../DomainEventJsonDeserializer";
import { RabbitMqConnection } from "./RabbitMqConnection";
import { RabbitMQConsumer } from "./RabbitMQConsumer";

/**
 * Fábrica para construir instancias de RabbitMQConsumer.
 */
export class RabbitMQConsumerFactory {
  /**
   * Crea una instancia de RabbitMQConsumerFactory.
   * @param {DomainEventDeserializer} deserializer - Deserializador de eventos de dominio.
   * @param {RabbitMqConnection} connection - Conexión a RabbitMQ.
   * @param {number} maxRetries - Número máximo de reintentos para procesar un mensaje.
   */
  constructor(
    private deserializer: DomainEventDeserializer,
    private connection: RabbitMqConnection,
    private maxRetries: Number
  ) {}

  /**
   * Construye una instancia de RabbitMQConsumer.
   * @param {DomainEventSubscriber<DomainEvent>} subscriber - Suscriptor de eventos de dominio.
   * @param {string} exchange - Nombre del intercambio de mensajes.
   * @param {string} queueName - Nombre de la cola de mensajes.
   * @returns {RabbitMQConsumer} Una instancia de RabbitMQConsumer configurada según los parámetros proporcionados.
   */
  build(
    subscriber: IDomainEventSubscriber<DomainEvent>,
    exchange: string,
    queueName: string
  ) {
    return new RabbitMQConsumer({
      subscriber,
      deserializer: this.deserializer,
      connection: this.connection,
      queueName,
      exchange,
      maxRetries: this.maxRetries,
    });
  }
}
