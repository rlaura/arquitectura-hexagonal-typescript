import { ConsumeMessage } from "amqplib";
import { RabbitMqConnection } from "./RabbitMqConnection";
import { DomainEvent } from "../../../../domain/bus/event/DomainEvent";
import { IDomainEventSubscriber } from "../../../../domain/bus/event/IDomainEventSubscriber";
import { DomainEventDeserializer } from "../DomainEventJsonDeserializer";

export class RabbitMQConsumer {
  private subscriber: IDomainEventSubscriber<DomainEvent>;
  private deserializer: DomainEventDeserializer;
  private connection: RabbitMqConnection;
  private maxRetries: Number;
  private queueName: string;
  private exchange: string;

  /**
   * Crea una instancia de RabbitMQConsumer.
   * @param {object} params - Parámetros de configuración.
   * @param {DomainEventSubscriber<DomainEvent>} params.subscriber - El suscriptor de eventos de dominio.
   * @param {DomainEventDeserializer} params.deserializer - Deserializador de eventos de dominio.
   * @param {RabbitMqConnection} params.connection - Conexión a RabbitMQ.
   * @param {string} params.queueName - Nombre de la cola de mensajes.
   * @param {string} params.exchange - Nombre del intercambio de mensajes.
   * @param {number} params.maxRetries - Número máximo de reintentos para procesar el mensaje.
   */
  constructor(params: {
    subscriber: IDomainEventSubscriber<DomainEvent>;
    deserializer: DomainEventDeserializer;
    connection: RabbitMqConnection;
    queueName: string;
    exchange: string;
    maxRetries: Number;
  }) {
    this.subscriber = params.subscriber;
    this.deserializer = params.deserializer;
    this.connection = params.connection;
    this.maxRetries = params.maxRetries;
    this.queueName = params.queueName;
    this.exchange = params.exchange;
  }

  /**
   * Método que maneja el mensaje recibido y lo procesa.
   * @param {ConsumeMessage} message - El mensaje recibido.
   * @returns {Promise<void>} Una promesa que se resuelve cuando se ha manejado el mensaje.
   */
  async onMessage(message: ConsumeMessage) {
    const content = message.content.toString();
    const domainEvent = this.deserializer.deserialize(content);

    try {
      await this.subscriber.on(domainEvent);
    } catch (error) {
      await this.handleError(message);
    } finally {
      this.connection.ack(message);
    }
  }

  /**
   * Maneja los errores que puedan ocurrir al procesar el mensaje.
   * @param {ConsumeMessage} message - El mensaje que causó el error.
   * @returns {Promise<void>} Una promesa que se resuelve cuando se ha manejado el error.
   */
  private async handleError(message: ConsumeMessage) {
    if (this.hasBeenRedeliveredTooMuch(message)) {
      await this.deadLetter(message);
    } else {
      await this.retry(message);
    }
  }

  /**
   * Reintenta el procesamiento del mensaje.
   * @param {ConsumeMessage} message - El mensaje a reintentar.
   * @returns {Promise<void>} Una promesa que se resuelve cuando se ha reintentado el procesamiento del mensaje.
   */
  private async retry(message: ConsumeMessage) {
    await this.connection.retry(message, this.queueName, this.exchange);
  }

  /**
   * Envía el mensaje a la cola de carta muerta.
   * @param {ConsumeMessage} message - El mensaje a enviar a la cola de carta muerta.
   * @returns {Promise<void>} Una promesa que se resuelve cuando se ha enviado el mensaje a la cola de carta muerta.
   */
  private async deadLetter(message: ConsumeMessage) {
    await this.connection.deadLetter(message, this.queueName, this.exchange);
  }

  /**
   * Verifica si el mensaje ha sido reintentado demasiadas veces.
   * @param {ConsumeMessage} message - El mensaje a verificar.
   * @returns {boolean} true si el mensaje ha sido reintentado demasiadas veces, de lo contrario false.
   */
  private hasBeenRedeliveredTooMuch(message: ConsumeMessage) {
    // if (this.hasBeenRedelivered(message)) {
    //   const count = parseInt(message.properties?.headers?.["redelivery_count"]);
    //   return count >= this.maxRetries;
    // }
    if (this.hasBeenRedelivered(message)) {
      // Convertir a número primitivo
      const count = Number(message.properties?.headers?.["redelivery_count"]);
      // Convertir a número primitivo
      const maxRetries = Number(this.maxRetries);
      // Verificar si count es un número válido
      return !isNaN(count) && count >= maxRetries;
    }
    return false;
  }

  /**
   * Verifica si el mensaje ha sido reintentado.
   * @param {ConsumeMessage} message - El mensaje a verificar.
   * @returns {boolean} true si el mensaje ha sido reintentado, de lo contrario false.
   */
  private hasBeenRedelivered(message: ConsumeMessage) {
    return message.properties?.headers?.["redelivery_count"] !== undefined;
  }
}
