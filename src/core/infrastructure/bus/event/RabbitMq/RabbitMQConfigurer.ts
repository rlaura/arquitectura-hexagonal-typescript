import { DomainEvent } from "../../../../domain/bus/event/DomainEvent";
import { IDomainEventSubscriber } from "../../../../domain/bus/event/IDomainEventSubscriber";
import { RabbitMqConnection } from "./RabbitMqConnection";
import { RabbitMQExchangeNameFormatter } from "./RabbitMQExchangeNameFormatter";
import { RabbitMQqueueFormatter } from "./RabbitMQqueueFormatter";

/**
 * Clase para configurar intercambios y colas en RabbitMQ para suscriptores de eventos de dominio.
 */
export class RabbitMQConfigurer {
  /**
   * Crea una instancia de RabbitMQConfigurer.
   * @param {RabbitMqConnection} connection - La conexión a RabbitMQ.
   * @param {RabbitMQqueueFormatter} queueNameFormatter - Formateador de nombres de colas RabbitMQ.
   * @param {number} messageRetryTtl - Tiempo de vida del mensaje de reintento.
   */
  constructor(
    private connection: RabbitMqConnection,
    private queueNameFormatter: RabbitMQqueueFormatter,
    private messageRetryTtl: number
  ) {}

  /**
   * Configura los intercambios y colas necesarios para los suscriptores de eventos de dominio.
   * @param {object} params - Parámetros de configuración.
   * @param {string} params.exchange - Nombre del intercambio.
   * @param {Array<DomainEventSubscriber<DomainEvent>>} params.subscribers - Suscriptores de eventos de dominio.
   * @return {Promise<void>} Una promesa que se resuelve cuando se han configurado los intercambios y colas.
   */
  async configure(params: {
    exchange: string;
    subscribers: Array<IDomainEventSubscriber<DomainEvent>>;
  }): Promise<void> {
    const retryExchange = RabbitMQExchangeNameFormatter.retry(params.exchange);
    const deadLetterExchange = RabbitMQExchangeNameFormatter.deadLetter(
      params.exchange
    );

    await this.connection.exchange({ name: params.exchange });
    await this.connection.exchange({ name: retryExchange });
    await this.connection.exchange({ name: deadLetterExchange });

    for (const subscriber of params.subscribers) {
      await this.addQueue(subscriber, params.exchange);
    }
  }

  /**
   * Agrega una cola y sus colas asociadas (de reintento y carta muerta) para un suscriptor de eventos de dominio.
   * @param {DomainEventSubscriber<DomainEvent>} subscriber - El suscriptor de eventos de dominio.
   * @param {string} exchange - Nombre del intercambio.
   * @return {Promise<void>} Una promesa que se resuelve cuando se ha agregado la cola y sus colas asociadas.
   */
  private async addQueue(
    subscriber: IDomainEventSubscriber<DomainEvent>,
    exchange: string
  ) {
    const retryExchange = RabbitMQExchangeNameFormatter.retry(exchange);
    const deadLetterExchange =
      RabbitMQExchangeNameFormatter.deadLetter(exchange);

    const routingKeys = this.getRoutingKeysFor(subscriber);

    const queue = this.queueNameFormatter.format(subscriber);
    const deadLetterQueue =
      this.queueNameFormatter.formatDeadLetter(subscriber);
    const retryQueue = this.queueNameFormatter.formatRetry(subscriber);

    await this.connection.queue({ routingKeys, name: queue, exchange });
    await this.connection.queue({
      routingKeys: [queue],
      name: retryQueue,
      exchange: retryExchange,
      messageTtl: this.messageRetryTtl,
      deadLetterExchange: exchange,
      deadLetterQueue: queue,
    });
    await this.connection.queue({
      routingKeys: [queue],
      name: deadLetterQueue,
      exchange: deadLetterExchange,
    });
  }

  /**
   * Obtiene las claves de enrutamiento para un suscriptor de eventos de dominio.
   * @param {DomainEventSubscriber<DomainEvent>} subscriber - El suscriptor de eventos de dominio.
   * @return {string[]} Las claves de enrutamiento para el suscriptor.
   */
  private getRoutingKeysFor(subscriber: IDomainEventSubscriber<DomainEvent>) {
    const routingKeys = subscriber
      .subscribedTo()
      .map((event) => event.EVENT_NAME);

    const queue = this.queueNameFormatter.format(subscriber);
    routingKeys.push(queue);

    return routingKeys;
  }
}
