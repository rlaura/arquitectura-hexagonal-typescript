import { DomainEvent } from "../../../../domain/bus/event/DomainEvent";
import { IEventBus } from "../../../../domain/bus/event/IEventBus";
import { DomainEventFailoverPublisher } from "../DomainEventFailoverPublisher/DomainEventFailoverPublisher";
import { DomainEventDeserializer } from "../DomainEventJsonDeserializer";
import { DomainEventJsonSerializer } from "../DomainEventJsonSerializer";
import { DomainEventSubscribers } from "../DomainEventSubscribers";
import { RabbitMqConnection } from "./RabbitMqConnection";
import { RabbitMQConsumerFactory } from "./RabbitMQConsumerFactory";
import { RabbitMQqueueFormatter } from "./RabbitMQqueueFormatter";

/**
 * Implementación de un bus de eventos utilizando RabbitMQ.
 */
export class RabbitMQEventBus implements IEventBus {
  private failoverPublisher: DomainEventFailoverPublisher;
  private connection: RabbitMqConnection;
  private exchange: string;
  private queueNameFormatter: RabbitMQqueueFormatter;
  private maxRetries: Number;

  /**
   * Crea una instancia de RabbitMQEventBus.
   * @param {Object} params - Parámetros de configuración para el bus de eventos.
   * @param {DomainEventFailoverPublisher} params.failoverPublisher - Publicador de eventos de failover.
   * @param {RabbitMqConnection} params.connection - Conexión a RabbitMQ.
   * @param {string} params.exchange - Nombre del intercambio de mensajes.
   * @param {RabbitMQqueueFormatter} params.queueNameFormatter - Formateador de nombres de cola de RabbitMQ.
   * @param {number} params.maxRetries - Número máximo de reintentos para publicar un evento.
   */
  constructor(params: {
    failoverPublisher: DomainEventFailoverPublisher;
    connection: RabbitMqConnection;
    exchange: string;
    queueNameFormatter: RabbitMQqueueFormatter;
    maxRetries: Number;
  }) {
    const { failoverPublisher, connection, exchange } = params;
    this.failoverPublisher = failoverPublisher;
    this.connection = connection;
    this.exchange = exchange;
    this.queueNameFormatter = params.queueNameFormatter;
    this.maxRetries = params.maxRetries;
  }

  /**
   * Agrega suscriptores al bus de eventos.
   * @param {DomainEventSubscribers} subscribers - Lista de suscriptores de eventos de dominio.
   * @returns {Promise<void>} Promesa que se resuelve cuando se agregan los suscriptores correctamente.
   */
  async addSubscribers(subscribers: DomainEventSubscribers): Promise<void> {
    const deserializer = DomainEventDeserializer.configure(subscribers);
    const consumerFactory = new RabbitMQConsumerFactory(
      deserializer,
      this.connection,
      this.maxRetries
    );

    for (const subscriber of subscribers.items) {
      const queueName = this.queueNameFormatter.format(subscriber);
      const rabbitMQConsumer = consumerFactory.build(
        subscriber,
        this.exchange,
        queueName
      );

      await this.connection.consume(
        queueName,
        rabbitMQConsumer.onMessage.bind(rabbitMQConsumer)
      );
    }
  }

  /**
   * Publica eventos en el bus de eventos.
   * @param {DomainEvent[]} events - Lista de eventos a publicar.
   * @returns {Promise<void>} Promesa que se resuelve cuando los eventos se publican correctamente.
   */
  async publish(events: Array<DomainEvent>): Promise<void> {
    for (const event of events) {
      try {
        const routingKey = event.eventName;
        const content = this.toBuffer(event);
        const options = this.options(event);

        await this.connection.publish({
          exchange: this.exchange,
          routingKey,
          content,
          options,
        });
      } catch (error: any) {
        await this.failoverPublisher.publish(event);
      }
    }
  }

  /**
   * Obtiene las opciones para la publicación de un evento.
   * @param {DomainEvent} event - El evento a ser publicado.
   * @returns {Object} - Las opciones para la publicación del evento.
   * @private
   */
  private options(event: DomainEvent) {
    return {
      messageId: event.eventId,
      contentType: "application/json",
      contentEncoding: "utf-8",
    };
  }

  /**
   * Convierte un evento de dominio en un búfer para su publicación.
   * @param {DomainEvent} event - El evento de dominio a convertir en un búfer.
   * @returns {Buffer} - El evento de dominio convertido en un búfer.
   * @private
   */
  private toBuffer(event: DomainEvent): Buffer {
    const eventPrimitives = DomainEventJsonSerializer.serialize(event);

    return Buffer.from(eventPrimitives);
  }
}
