import amqplib, { ConsumeMessage } from "amqplib";
import { ConnectionSettings } from "./ConnectionSettings";
import { RabbitMQExchangeNameFormatter } from "./RabbitMQExchangeNameFormatter";
/**
 * Clase para gestionar la conexión y operaciones con RabbitMQ.
 */
export class RabbitMqConnection {
  private connectionSettings: ConnectionSettings;
  private channel?: amqplib.ConfirmChannel;
  private connection?: amqplib.Connection;

  /**
   * Crea una nueva instancia de RabbitMqConnection.
   * @param {ConnectionSettings} params - Configuración de la conexión.
   * @return {RabbitMqConnection} Una nueva instancia de RabbitMqConnection.
   */
  constructor(params: { connectionSettings: ConnectionSettings }) {
    this.connectionSettings = params.connectionSettings;
  }

  /**
   * Establece la conexión con el servidor RabbitMQ y crea un canal.
   */
  async connect() {
    this.connection = await this.amqpConnect();
    this.channel = await this.amqpChannel();
  }

  /**
   * Declara un intercambio en el servidor RabbitMQ.
   * @param {string} name - Nombre del intercambio.
   * @return {Promise<void>} Una promesa que se resuelve cuando se ha declarado el intercambio.
   */
  async exchange(params: { name: string }) {
    return await this.channel?.assertExchange(params.name, "topic", {
      durable: true,
    });
  }

  /**
   * Declara una cola en el servidor RabbitMQ.
   * @param {QueueParams} params - Parámetros para la declaración de la cola.
   * @return {Promise<void>} Una promesa que se resuelve cuando se ha declarado la cola.
   */
  async queue(params: {
    exchange: string;
    name: string;
    routingKeys: string[];
    deadLetterExchange?: string;
    deadLetterQueue?: string;
    messageTtl?: Number;
  }) {
    const durable = true;
    const exclusive = false;
    const autoDelete = false;
    const args = this.getQueueArguments(params);

    await this.channel?.assertQueue(params.name, {
      exclusive,
      durable,
      autoDelete,
      arguments: args,
    });
    for (const routingKey of params.routingKeys) {
      await this.channel!.bindQueue(params.name, params.exchange, routingKey);
    }
  }

  /**
   * Obtiene los argumentos de configuración de la cola RabbitMQ.
   * @param {QueueParams} params - Parámetros de declaración de la cola.
   * @return {any} Los argumentos de configuración de la cola.
   */
  private getQueueArguments(params: {
    exchange: string;
    name: string;
    routingKeys: string[];
    deadLetterExchange?: string;
    deadLetterQueue?: string;
    messageTtl?: Number;
  }) {
    let args: any = {};
    if (params.deadLetterExchange) {
      args = { ...args, "x-dead-letter-exchange": params.deadLetterExchange };
    }
    if (params.deadLetterQueue) {
      args = { ...args, "x-dead-letter-routing-key": params.deadLetterQueue };
    }
    if (params.messageTtl) {
      args = { ...args, "x-message-ttl": params.messageTtl };
    }

    return args;
  }
  /**
   * Elimina una cola del servidor RabbitMQ.
   * @param {string} queue - Nombre de la cola a eliminar.
   * @return {Promise<void>} Una promesa que se resuelve cuando se ha eliminado la cola.
   */
  async deleteQueue(queue: string) {
    return await this.channel!.deleteQueue(queue);
  }

  /**
   * Establece la conexión con el servidor RabbitMQ.
   * @return {Promise<Connection>} Una promesa que se resuelve con la conexión establecida.
   */
  private async amqpConnect() {
    const { hostname, port, secure } = this.connectionSettings.connection;
    const { username, password, vhost } = this.connectionSettings;
    const protocol = secure ? "amqps" : "amqp";

    const connection = await amqplib.connect({
      protocol,
      hostname,
      port,
      username,
      password,
      vhost,
    });

    connection.on("error", (err: any) => {
      Promise.reject(err);
    });

    return connection;
  }

  /**
   * Crea un canal en la conexión RabbitMQ.
   * @return {Promise<ConfirmChannel>} Una promesa que se resuelve con el canal creado.
   */
  private async amqpChannel(): Promise<amqplib.ConfirmChannel> {
    const channel = await this.connection!.createConfirmChannel();
    await channel.prefetch(1);

    return channel;
  }

  /**
   * Publica un mensaje en un intercambio en el servidor RabbitMQ.
   * @param {PublishParams} params - Parámetros para la publicación del mensaje.
   * @return {Promise<void>} Una promesa que se resuelve cuando se ha publicado el mensaje.
   */
  async publish(params: {
    exchange: string;
    routingKey: string;
    content: Buffer;
    options: {
      messageId: string;
      contentType: string;
      contentEncoding: string;
      priority?: number;
      headers?: any;
    };
  }) {
    const { routingKey, content, options, exchange } = params;

    return new Promise((resolve: Function, reject: Function) => {
      this.channel!.publish(
        exchange,
        routingKey,
        content,
        options,
        (error: any) => (error ? reject(error) : resolve())
      );
    });
  }

  /**
   * Cierra el canal y la conexión RabbitMQ.
   * @return {Promise<void>} Una promesa que se resuelve cuando se han cerrado el canal y la conexión.
   */
  async close() {
    await this.channel?.close();
    return await this.connection?.close();
  }

  /**
   * Consume mensajes de una cola RabbitMQ.
   * @param {string} queue - Nombre de la cola de la que consumir mensajes.
   * @param {(message: ConsumeMessage) => {}} onMessage - Función de callback para procesar los mensajes consumidos.
   * @return {Promise<void>} Una promesa que se resuelve cuando se han consumido los mensajes.
   */
  async consume(queue: string, onMessage: (message: ConsumeMessage) => {}) {
    await this.channel!.consume(queue, (message: ConsumeMessage | null) => {
      if (!message) {
        return;
      }
      onMessage(message);
    });
  }

  /**
   * Confirma la recepción de un mensaje consumido.
   * @param {ConsumeMessage} message - El mensaje a confirmar.
   */
  ack(message: ConsumeMessage) {
    this.channel!.ack(message);
  }

  /**
   * Publica un mensaje en la cola de reintento.
   * @param {ConsumeMessage} message - El mensaje a reintentar.
   * @param {string} queue - Nombre de la cola de reintento.
   * @param {string} exchange - Nombre del intercambio.
   * @return {Promise<void>} Una promesa que se resuelve cuando se ha publicado el mensaje en la cola de reintento.
   */
  async retry(message: ConsumeMessage, queue: string, exchange: string) {
    const retryExchange = RabbitMQExchangeNameFormatter.retry(exchange);
    const options = this.getMessageOptions(message);

    return await this.publish({
      exchange: retryExchange,
      routingKey: queue,
      content: message.content,
      options,
    });
  }

  /**
   * Publica un mensaje en la cola de carta muerta.
   * @param {ConsumeMessage} message - El mensaje a enviar a la carta muerta.
   * @param {string} queue - Nombre de la cola de la carta muerta.
   * @param {string} exchange - Nombre del intercambio.
   * @return {Promise<void>} Una promesa que se resuelve cuando se ha publicado el mensaje en la carta muerta.
   */
  async deadLetter(message: ConsumeMessage, queue: string, exchange: string) {
    const deadLetterExchange =
      RabbitMQExchangeNameFormatter.deadLetter(exchange);
    const options = this.getMessageOptions(message);

    return await this.publish({
      exchange: deadLetterExchange,
      routingKey: queue,
      content: message.content,
      options,
    });
  }

  /**
   * Obtiene las opciones del mensaje a partir de sus propiedades.
   * @param {ConsumeMessage} message - El mensaje del que obtener las opciones.
   * @return {MessageProperties} Las opciones del mensaje.
   */
  private getMessageOptions(message: ConsumeMessage) {
    const { messageId, contentType, contentEncoding, priority } =
      message.properties;
    const options = {
      messageId,
      headers: this.incrementRedeliveryCount(message),
      contentType,
      contentEncoding,
      priority,
    };
    return options;
  }

  /**
   * Incrementa el contador de reintentos en las cabeceras del mensaje.
   * @param {ConsumeMessage} message - El mensaje al que incrementar el contador.
   * @return {any} Las cabeceras del mensaje con el contador incrementado.
   */
  private incrementRedeliveryCount(message: ConsumeMessage) {
    // Verifica si message.properties y message.properties.headers están definidos
    if (message.properties && message.properties.headers) {
      if (this.hasBeenRedelivered(message)) {
        // Verifica si "redelivery_count" está definido antes de intentar acceder a su valor
        const count = parseInt(message.properties.headers["redelivery_count"]);
        message.properties.headers["redelivery_count"] = count + 1;
      } else {
        message.properties.headers["redelivery_count"] = 1;
      }

      return message.properties.headers;
    } else {
      // Si message.properties o message.properties.headers son undefined, devuelve un valor predeterminado
      return {};
    }
  }

  /**
   * Verifica si el mensaje ha sido reintentado previamente.
   * @param {ConsumeMessage} message - El mensaje a verificar.
   * @return {boolean} `true` si el mensaje ha sido reintentado, `false` en caso contrario.
   */
  private hasBeenRedelivered(message: ConsumeMessage) {
    return message.properties?.headers?.["redelivery_count"] !== undefined;
  }
}
