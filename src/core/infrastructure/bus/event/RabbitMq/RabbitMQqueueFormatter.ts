import { DomainEvent } from "../../../../domain/bus/event/DomainEvent";
import { IDomainEventSubscriber } from "../../../../domain/bus/event/IDomainEventSubscriber";

/**
 * Clase para formatear nombres de colas RabbitMQ.
 */
export class RabbitMQqueueFormatter {
  /**
   * Crea una nueva instancia de RabbitMQqueueFormatter.
   * @param {string} moduleName - El nombre del módulo.
   * @return {RabbitMQqueueFormatter} Una nueva instancia de RabbitMQqueueFormatter.
   */
  constructor(private moduleName: string) {}
  
  /**
   * Formatea el nombre de la cola para un suscriptor de eventos de dominio.
   * @param {DomainEventSubscriber<DomainEvent>} subscriber - El suscriptor de eventos de dominio.
   * @return {string} El nombre de la cola formateado.
   */
  format(subscriber: IDomainEventSubscriber<DomainEvent>) {
    const value = subscriber.constructor.name;
    const name = value
      .split(/(?=[A-Z])/)
      .join('_')
      .toLowerCase();
    return `${this.moduleName}.${name}`;
  }

  /**
   * Formatea el nombre de la cola de reintento para un suscriptor de eventos de dominio.
   * @param {DomainEventSubscriber<DomainEvent>} subscriber - El suscriptor de eventos de dominio.
   * @return {string} El nombre de la cola de reintento formateado.
   */
  formatRetry(subscriber: IDomainEventSubscriber<DomainEvent>) {
    const name = this.format(subscriber);
    return `retry.${name}`;
  }
  
  /**
   * Formatea el nombre de la cola de carta muerta para un suscriptor de eventos de dominio.
   * @param {DomainEventSubscriber<DomainEvent>} subscriber - El suscriptor de eventos de dominio.
   * @return {string} El nombre de la cola de carta muerta formateado.
   */
  formatDeadLetter(subscriber: IDomainEventSubscriber<DomainEvent>) {
    const name = this.format(subscriber);
    return `dead_letter.${name}`;
  }
}
