import { DomainEvent } from "../../../domain/bus/event/DomainEvent";

/**
 * Clase para serializar eventos de dominio a formato JSON.
 */
export class DomainEventJsonSerializer {
  /**
   * Serializa un evento de dominio a formato JSON.
   * @param {DomainEvent} event - El evento de dominio a serializar.
   * @return {string} El evento de dominio serializado en formato JSON.
   */
  static serialize(event: DomainEvent): string {
    return JSON.stringify({
      data: {
        id: event.eventId,
        type: event.eventName,
        occurred_on: event.occurredOn.toISOString(),
        aggregateId: event.aggregateId,
        attributes: event.toPrimitives()
      }
    });
  }
}
