import { DomainEventClass } from "../../../domain/bus/event/DomainEvent";
import { DomainEventSubscribers } from "./DomainEventSubscribers";

/**
 * Representa un evento de dominio en formato JSON.
 */
type DomainEventJSON = {
  type: string;
  aggregateId: string;
  attributes: string;
  id: string;
  occurred_on: string;
};

export class DomainEventDeserializer extends Map<string, DomainEventClass> {
  /**
   * Configura el deserializador de eventos de dominio con los suscriptores proporcionados.
   * @param {DomainEventSubscribers} subscribers - Los suscriptores para configurar el deserializador.
   * @return {DomainEventDeserializer} El deserializador de eventos de dominio configurado.
   */
  static configure(subscribers: DomainEventSubscribers) {
    const mapping = new DomainEventDeserializer();
    subscribers.items.forEach(subscriber => {
      subscriber.subscribedTo().forEach(mapping.registerEvent.bind(mapping));
    });

    return mapping;
  }
  /**
   * Registra un evento de dominio en el deserializador.
   * @param {DomainEventClass} domainEvent - El evento de dominio para registrar.
   */
  private registerEvent(domainEvent: DomainEventClass) {
    const eventName = domainEvent.EVENT_NAME;
    this.set(eventName, domainEvent);
  }

  /**
   * Deserializa un evento de dominio desde su representación en JSON.
   * @param {string} event - El evento de dominio en formato JSON.
   * @return {DomainEvent} El evento de dominio deserializado.
   * @throws {Error} Se lanza si no se encuentra la clase de evento de dominio correspondiente en el mapeo.
   */
  deserialize(event: string) {
    const eventData = JSON.parse(event).data as DomainEventJSON;
    const { type, aggregateId, attributes, id, occurred_on } = eventData;
    const eventClass = super.get(type);

    if (!eventClass) {
      throw Error(`DomainEvent mapping not found for event ${type}`);
    }

    return eventClass.fromPrimitives({
      aggregateId,
      attributes,
      occurredOn: new Date(occurred_on),
      eventId: id
    });
  }
}
