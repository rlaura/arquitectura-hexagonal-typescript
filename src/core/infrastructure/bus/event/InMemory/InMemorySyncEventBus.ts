import { DomainEvent } from "../../../../domain/bus/event/DomainEvent";
import { DomainEventMapping } from "../../../../domain/bus/event/DomainEventMapping";
import { IDomainEventSubscriber } from "../../../../domain/bus/event/IDomainEventSubscriber";
import { IEventBus } from "../../../../domain/bus/event/IEventBus";

type Subscription = {
  boundedCallback: Function;
  originalCallback: Function;
};

/**
 * Clase que representa un bus de eventos sincrónico en memoria.
 */
export class InMemorySyncEventBus implements IEventBus {
  // Mapa que almacena las suscripciones de eventos por nombre del evento.
  private subscriptions: Map<string, Array<Subscription>>;
  /**
   * Constructor de la clase.
   * Inicializa el mapa de suscripciones.
   */
  constructor() {
    this.subscriptions = new Map();
  }

  /**
   * Método para iniciar el bus de eventos.
   * No realiza ninguna acción en esta implementación.
   * @returns Una promesa vacía.
   */
  async start(): Promise<void> {}

  /**
   * Método para publicar eventos en el bus de eventos.
   * @param events Los eventos de dominio que se van a publicar.
   * @returns Una promesa que se resuelve cuando se completa la publicación de los eventos.
   */
  async publish(events: Array<DomainEvent>): Promise<void> {
    // Array para almacenar las ejecuciones de los callbacks de los suscriptores.
    const executions: any = [];
    // Itera sobre cada evento.
    events.map((event) => {
      // Obtiene los suscriptores del evento actual.
      const subscribers = this.subscriptions.get(event.eventName);
      if (subscribers) {
        // Ejecuta el callback de cada suscriptor y almacena la promesa resultante en el array de ejecuciones.
        return subscribers.map((subscriber) =>
          executions.push(subscriber.boundedCallback(event))
        );
      }
    });
    // Espera a que todas las promesas de ejecución se completen.
    await Promise.all(executions);
  }

  /**
   * Método para agregar suscriptores al bus de eventos.
   * @param subscribers Los suscriptores de eventos de dominio que se van a agregar al bus de eventos.
   */
  addSubscribers(subscribers: Array<IDomainEventSubscriber<DomainEvent>>) {
    // Itera sobre cada suscriptor y sus eventos suscritos, y los agrega al bus de eventos.
    subscribers.map((subscriber) =>
      subscriber
        .subscribedTo()
        .map((event) => this.subscribe(event.EVENT_NAME!, subscriber))
    );
  }
  /**
   * Método para establecer el mapeo de eventos de dominio en el bus de eventos.
   * No realiza ninguna acción en esta implementación.
   * @param domainEventMapping El mapeo de eventos de dominio que se va a establecer.
   */
  setDomainEventMapping(domainEventMapping: DomainEventMapping): void {}

  /**
   * Método privado para suscribir un callback de suscriptor a un evento específico.
   * @param topic El nombre del evento al que se suscribe el suscriptor.
   * @param subscriber El suscriptor que se suscribe al evento.
   */
  private subscribe(
    topic: string,
    subscriber: IDomainEventSubscriber<DomainEvent>
  ): void {
    // Obtiene las suscripciones actuales para el evento.
    const currentSubscriptions = this.subscriptions.get(topic);
    // Crea una nueva suscripción
    const subscription = {
      boundedCallback: subscriber.on.bind(subscriber),
      originalCallback: subscriber.on,
    };
    // Si ya hay suscripciones para el evento, agrega la nueva suscripción. De lo contrario, crea un nuevo array de suscripciones.
    if (currentSubscriptions) {
      currentSubscriptions.push(subscription);
    } else {
      this.subscriptions.set(topic, [subscription]);
    }
  }
}
