import { decorate, injectable } from "inversify"
import { EventEmitter } from 'events';

import { DomainEvent } from '../../../../domain/bus/event/DomainEvent';
import { IEventBus } from '../../../../domain/bus/event/IEventBus';
import { DomainEventSubscribers } from '../DomainEventSubscribers';

/**
 * Implementación de un bus de eventos asíncrono en memoria.
 */
export class InMemoryAsyncEventBus extends EventEmitter implements IEventBus {
  /**
   * Publica eventos en el bus de eventos.
   * @param {DomainEvent[]} events - Los eventos a publicar.
   * @return {Promise<void>} Una promesa que se resuelve una vez que los eventos se han publicado correctamente.
   */
  async publish(events: DomainEvent[]): Promise<void> {
      events.forEach(event => this.emit(event.eventName, event));
  }

  /**
   * Agrega suscriptores al bus de eventos.
   * @param {DomainEventSubscribers} subscribers - Los suscriptores a agregar.
   */
  addSubscribers(subscribers: DomainEventSubscribers): void {
      subscribers.items.forEach(subscriber => {
          subscriber.subscribedTo().forEach(event => {
              this.on(event.EVENT_NAME, subscriber.on.bind(subscriber));
          });
      });
  }
}