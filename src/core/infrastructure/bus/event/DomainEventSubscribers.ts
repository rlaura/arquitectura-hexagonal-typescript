import { ContainerBuilder, Definition } from 'node-dependency-injection';
import { DomainEvent } from "../../../domain/bus/event/DomainEvent";
import { IDomainEventSubscriber } from "../../../domain/bus/event/IDomainEventSubscriber";

/**
 * Representa un conjunto de suscriptores de eventos de dominio.
 */
export class DomainEventSubscribers {
  /**
   * Crea una nueva instancia de DomainEventSubscribers.
   * @param {IDomainEventSubscriber<DomainEvent>[]} items - Los suscriptores de eventos de dominio.
   * @return {DomainEventSubscribers} Una nueva instancia de DomainEventSubscribers.
   */
  constructor(public items: Array<IDomainEventSubscriber<DomainEvent>>) {}

  /**
   * Crea una instancia de DomainEventSubscribers a partir de un contenedor de servicios.
   * @param {ContainerBuilder} container - El contenedor de servicios que contiene los suscriptores de eventos.
   * @return {DomainEventSubscribers} Una nueva instancia de DomainEventSubscribers.
   */
  static from(container: ContainerBuilder): DomainEventSubscribers {
    const subscriberDefinitions = container.findTaggedServiceIds('domainEventSubscriber') as Map<String, Definition>;
    const subscribers: Array<IDomainEventSubscriber<DomainEvent>> = [];

    subscriberDefinitions.forEach((value: Definition, key: String) => {
      const domainEventSubscriber = container.get<IDomainEventSubscriber<DomainEvent>>(key.toString());
      subscribers.push(domainEventSubscriber);
    });

    return new DomainEventSubscribers(subscribers);
  }
}
