import { EventEmitter } from 'events';
import { DomainEvent } from '../../../domain/bus/event/DomainEvent';
import { IDomainEventSubscriber } from '../../../domain/bus/event/IDomainEventSubscriber';
/**
 * Clase que representa un bus de eventos basado en EventEmitter.
 */
export class EventEmitterBus extends EventEmitter {

  /**
   * Constructor de la clase.
   * @param subscribers Los suscriptores de eventos de dominio que se agregarán al bus de eventos.
   */
  constructor(subscribers: Array<IDomainEventSubscriber<DomainEvent>>) {
    super();
    // Registra los suscriptores al inicializar el bus.
    this.registerSubscribers(subscribers);
  }

  /**
   * Método para registrar suscriptores de eventos de dominio en el bus de eventos.
   * @param subscribers Los suscriptores que se van a registrar en el bus de eventos.
   */
  registerSubscribers(subscribers?: IDomainEventSubscriber<DomainEvent>[]) {
    // Si hay suscriptores, registra cada uno de ellos.
    subscribers?.map(subscriber => {
      this.registerSubscriber(subscriber);
    });
  }

   /**
   * Método privado para registrar un suscriptor de eventos de dominio en el bus de eventos.
   * @param subscriber El suscriptor que se va a registrar en el bus de eventos.
   */
  private registerSubscriber(subscriber: IDomainEventSubscriber<DomainEvent>) {
    /**
     * Por cada evento al que está suscrito el suscriptor, registra una función que 
     * llame al método on del suscriptor cuando ese evento ocurra.
     */
    subscriber.subscribedTo().map(event => {
      this.on(event.EVENT_NAME, subscriber.on.bind(subscriber));
    });
  }
  /**
   * Método para publicar eventos de dominio en el bus de eventos.
   * @param events Los eventos de dominio que se van a publicar en el bus de eventos.
   */
  publish(events: DomainEvent[]): void {
    // Publica cada evento en el bus de eventos, emitiendo un evento correspondiente.
    events.map(event => this.emit(event.eventName, event));
  }
}
