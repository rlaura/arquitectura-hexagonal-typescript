import { Collection, MongoClient } from "mongodb";
import { DomainEventJsonSerializer } from "../DomainEventJsonSerializer";
import { DomainEventDeserializer } from "../DomainEventJsonDeserializer";
import { DomainEvent } from "../../../../domain/bus/event/DomainEvent";

/**
 * Clase que maneja la publicación y consumo de eventos de dominio para el escenario de recuperación de fallos.
 */
export class DomainEventFailoverPublisher {
  /**
   * Nombre de la colección en la base de datos donde se almacenan los eventos de dominio para la recuperación de fallos.
   */
  static collectionName = "DomainEvents";

  /**
   * Crea una instancia de DomainEventFailoverPublisher.
   * @param {Promise<MongoClient>} client - Cliente de MongoDB utilizado para acceder a la base de datos.
   * @param {DomainEventDeserializer} [deserializer] - Deserializador opcional utilizado para convertir los eventos almacenados en documentos de MongoDB de vuelta a objetos de evento de dominio.
   */
  constructor(
    private client: Promise<MongoClient>,
    private deserializer?: DomainEventDeserializer
  ) {}

  /**
   * Obtiene la colección de MongoDB donde se almacenan los eventos de dominio para la recuperación de fallos.
   * @returns {Promise<Collection>} - La colección de MongoDB.
   * @protected
   */
  protected async collection(): Promise<Collection> {
    return (await this.client)
      .db()
      .collection(DomainEventFailoverPublisher.collectionName);
  }

  /**
   * Establece el deserializador utilizado para convertir los eventos almacenados en documentos de MongoDB de vuelta a objetos de evento de dominio.
   * @param {DomainEventDeserializer} deserializer - El deserializador a establecer.
   */
  setDeserializer(deserializer: DomainEventDeserializer) {
    this.deserializer = deserializer;
  }

  /**
   * Publica un evento de dominio almacenándolo en la base de datos para la recuperación de fallos.
   * @param {DomainEvent} event - El evento de dominio a publicar.
   * @returns {Promise<void>} - Promesa que se resuelve cuando se completa la publicación del evento.
   */
  async publish(event: DomainEvent): Promise<void> {
    const collection = await this.collection();

    const eventSerialized = DomainEventJsonSerializer.serialize(event);
    const options = { upsert: true };
    const update = { $set: { eventId: event.eventId, event: eventSerialized } };

    await collection.updateOne({ eventId: event.eventId }, update, options);
  }

  /**
   * Consume los eventos de dominio almacenados en la base de datos para la recuperación de fallos.
   * @returns {Promise<Array<DomainEvent>>} - Promesa que se resuelve con un array de eventos de dominio consumidos.
   * @throws {Error} - Si el deserializador no ha sido establecido.
   */
  async consume(): Promise<Array<DomainEvent>> {
    const collection = await this.collection();
    const documents = await collection.find().limit(200).toArray();
    if (!this.deserializer) {
      throw new Error("Deserializer has not been set yet");
    }

    const events = documents.map((document) =>
      this.deserializer!.deserialize(document.event)
    );

    return events.filter(Boolean) as Array<DomainEvent>;
  }
}
