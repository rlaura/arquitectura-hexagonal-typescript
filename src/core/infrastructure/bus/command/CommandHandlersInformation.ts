import { CommandNotRegisteredError } from "../../../domain/bus/command/CommandNotRegisteredError";
import { ICommand } from "../../../domain/bus/command/ICommand";
import { ICommandHandler } from "../../../domain/bus/command/ICommandHandler";

/**
 * se utiliza para gestionar y buscar manejadores de comandos en una aplicación.
 */
export class CommandHandlersInformation {
  /**
   * es un mapa que asigna instancias de comandos a sus respectivos manejadores de comandos.
   */
  private commandHandlersMap: Map<ICommand, ICommandHandler<ICommand>>;

  /**
   * toma una matriz de manejadores de comandos y los utiliza para inicializar 
   * la propiedad commandHandlersMap mediante el método formatHandlers.
   * @param commandHandlers Un array que contiene los manejadores de comandos que se utilizarán 
   *                        para inicializar el mapeo de comandos en la instancia de CommandHandlersInformation. 
   *                        Cada elemento del array debe ser una instancia de CommandHandler que maneje comandos 
   *                        del tipo genérico Command
   */
  constructor(commandHandlers: Array<ICommandHandler<ICommand>>) {
    this.commandHandlersMap = this.formatHandlers(commandHandlers);
  }
  /**
   * Método privado que formatea los manejadores de comandos en un mapa, 
   * donde la clave es la clase del comando y el valor es su manejador de comando correspondiente.
   * @param commandHandlers Un array que contiene los manejadores de comandos que se utilizarán 
   *                        para inicializar el mapeo de comandos en la instancia de CommandHandlersInformation. 
   *                        Cada elemento del array debe ser una instancia de CommandHandler que maneje comandos 
   *                        del tipo genérico Command
   * @returns Un mapa que asigna instancias de comandos a sus respectivos manejadores de comandos.
   */
  private formatHandlers(commandHandlers: Array<ICommandHandler<ICommand>>): Map<ICommand, ICommandHandler<ICommand>> {
    const handlersMap = new Map();
    /**
     * Itera sobre cada manejador de comando y los agrega al mapa commandHandlersMap.
     */
    commandHandlers.forEach(commandHandler => {
      handlersMap.set(commandHandler.subscribedTo(), commandHandler);
    });

    return handlersMap;
  }
  /**
   * Método público que busca el manejador correspondiente a un comando dado en el commandHandlersMap.
   * Si no se encuentra ningún manejador para el comando dado, se lanza una excepción CommandNotRegisteredError.
   * 
   * @param command El comando para el cual se busca el manejador correspondiente.
   * @returns El manejador de comando correspondiente al comando proporcionado.
   */
  public search(command: ICommand): ICommandHandler<ICommand> {
    const commandHandler = this.commandHandlersMap.get(command.constructor);

    if (!commandHandler) {
      throw new CommandNotRegisteredError(command);
    }

    return commandHandler;
  }
}
