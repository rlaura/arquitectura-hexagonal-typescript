import { ICommand } from "../../../domain/bus/command/ICommand";
import { ICommandBus } from "../../../domain/bus/command/ICommandBus";
import { CommandHandlers } from "./CommandHandlers";

/**
 * Implementación de un bus de comandos en memoria.
 */
export class InMemoryCommandBus implements ICommandBus {
  /**
   * Crea una nueva instancia de InMemoryCommandBus.
   * @param {CommandHandlers} commandHandlers - Los controladores de comandos asociados al bus.
   * @return InMemoryCommandBus Una nueva instancia de InMemoryCommandBus.
   */
  constructor(private commandHandlers: CommandHandlers) {}

  /**
   * Despacha un comando, manejándolo con el controlador de comando correspondiente.
   * @param {Command} command - El comando a despachar.
   * @return {Promise<void>} Una promesa que se resuelve una vez que el comando ha sido manejado.
   */
  async dispatch(command: ICommand): Promise<void> {
    const handler = this.commandHandlers.get(command);

    await handler.handle(command);
  }
}
