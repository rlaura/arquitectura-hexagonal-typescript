import { IQuery } from "../../../domain/bus/query/IQuery";
import { IQueryHandler } from "../../../domain/bus/query/IQueryHandler";
import { IResponse } from "../../../domain/bus/query/IResponse";
import { QueryNotRegisteredError } from "../../../domain/bus/query/QueryNotRegisteredError";

/**
 * Clase que gestiona los manejadores de consultas y proporciona métodos para buscar manejadores de consultas.
 */
export class QueryHandlersInformation {
  /**
   * Mapa que almacena los manejadores de consultas asociados a cada tipo de consulta.
   */
  private queryHandlersMap: Map<IQuery, IQueryHandler<IQuery, IResponse>>;
  /**
   * Constructor de la clase.
   * @param queryHandlers Un array de manejadores de consultas para inicializar el mapeo de manejadores.
   */
  constructor(queryHandlers: Array<IQueryHandler<IQuery, IResponse>>) {
    this.queryHandlersMap = this.formatHandlers(queryHandlers);
  }
  
  /**
   * Método privado que formatea los manejadores de consultas en un mapa.
   * @param queryHandlers Un array de manejadores de consultas.
   * @returns Un mapa que asigna instancias de consultas a sus respectivos manejadores.
   */
  private formatHandlers(
    queryHandlers: Array<IQueryHandler<IQuery, IResponse>>
  ): Map<IQuery, IQueryHandler<IQuery, Response>> {
    const handlersMap = new Map();
    // Itera sobre cada manejador de consulta y los agrega al mapa handlersMap.
    queryHandlers.forEach(queryHandler => {
      handlersMap.set(queryHandler.subscribedTo(), queryHandler);
    });

    return handlersMap;
  }
  
  /**
   * Método público para buscar el manejador correspondiente a una consulta dada.
   * @param query La consulta para la cual se busca el manejador correspondiente.
   * @returns El manejador de consulta correspondiente a la consulta proporcionada.
   * @throws QueryNotRegisteredError Si no se encuentra ningún manejador para la consulta dada.
   */
  public search(query: IQuery): IQueryHandler<IQuery, IResponse> {
    const queryHandler = this.queryHandlersMap.get(query.constructor);

    if (!queryHandler) {
      throw new QueryNotRegisteredError(query);
    }

    return queryHandler;
  }
}
