import { IQuery } from "../../../domain/bus/query/IQuery";
import { IQueryHandler } from "../../../domain/bus/query/IQueryHandler";
import { IResponse } from "../../../domain/bus/query/IResponse";
import { QueryNotRegisteredError } from "../../../domain/bus/query/QueryNotRegisteredError";

/**
 * Representa un mapa de controladores de consultas.
 */
export class QueryHandlers extends Map<
  IQuery,
  IQueryHandler<IQuery, IResponse>
> {
  /**
   * Crea una nueva instancia de QueryHandlers.
   * @param {Array<QueryHandler<Query, Response>>} queryHandlers - Los controladores de consultas para inicializar el mapa.
   * @return {QueryHandlers} Una nueva instancia de QueryHandlers.
   */
  constructor(queryHandlers: Array<IQueryHandler<IQuery, IResponse>>) {
    super();
    queryHandlers.forEach((queryHandler) => {
      this.set(queryHandler.subscribedTo(), queryHandler);
    });
  }
  /**
   * Obtiene el controlador de consulta asociado a una consulta.
   * @param {Query} query - La consulta para la que se busca el controlador.
   * @return {QueryHandler<Query, Response>} El controlador de consulta asociado a la consulta.
   * @throws {QueryNotRegisteredError} Se lanza si no se encuentra ningún controlador de consulta asociado a la consulta.
   */
  public get(query: IQuery): IQueryHandler<IQuery, IResponse> {
    const queryHandler = super.get(query.constructor);

    if (!queryHandler) {
      throw new QueryNotRegisteredError(query);
    }

    return queryHandler;
  }
}
