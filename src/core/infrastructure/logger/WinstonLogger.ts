import winston, { Logger as WinstonLoggerType } from 'winston';
import ILogger from '../../domain/logger/ILogger';
/**
 * Enumeración que define los niveles de log disponibles.
 * ❌, 🔥 error
 * 🐞, 🔎, 🔷, debug
 * ⚠️ , 🚧, ❗, 🚨 warning
 * ✅, ✔️ ok
 */
enum Levels {
  DEBUG = 'debug',
  ERROR = 'error',
  INFO = 'info'
}
/**
 * Clase que implementa un logger utilizando Winston.
 */
class WinstonLogger implements ILogger {
  // Instancia del logger de Winston.
  private logger: WinstonLoggerType;
  /**
   * Constructor de la clase.
   * Inicializa el logger de Winston con las configuraciones necesarias.
   */
  constructor() {
    this.logger = winston.createLogger({
      format: winston.format.combine(
        winston.format.prettyPrint(),
        winston.format.errors({ stack: true }),
        winston.format.splat(),
        winston.format.colorize(),
        winston.format.simple()
      ),
      transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: `logs/${Levels.DEBUG}.log`, level: Levels.DEBUG }),
        new winston.transports.File({ filename: `logs/${Levels.ERROR}.log`, level: Levels.ERROR }),
        new winston.transports.File({ filename: `logs/${Levels.INFO}.log`, level: Levels.INFO })
      ]
    });
  }
  /**
   * Método para registrar un mensaje de debug.
   * @param message El mensaje de debug que se va a registrar.
   */
  debug(message: string) {
    this.logger.debug(message);
  }

  /**
   * Método para registrar un mensaje de error.
   * @param message El mensaje de error que se va a registrar.
   */
  error(message: string | Error) {
    this.logger.error(message);
  }

  /**
   * Método para registrar un mensaje informativo.
   * @param message El mensaje informativo que se va a registrar.
   */
  info(message: string) {
    this.logger.info(message);
  }
}

// Exporta la clase WinstonLogger como predeterminada.
export default WinstonLogger;
