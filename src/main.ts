import { AppServer } from "./App";

try {
  new AppServer().start();
} catch (e) {
  console.log(e);
}
