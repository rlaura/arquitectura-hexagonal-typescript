import { Nullable } from "../../../../../../core/domain/value-objects/Nullable";
import { RegisterNewUser } from "../../../domain/entities/RegisterNewUser";
import { IRegisterNewUserRepository } from "../../../domain/repositories/IRegisterNewUserRepository";
import { RegisterNewUserId } from "../../../domain/value-objects/RegisterNewUserId";

export class PrismaRegisterNewUser implements IRegisterNewUserRepository {
  constructor() {}

  save(course: RegisterNewUser): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      console.log("se guardo un curso");
    });
  }

  search(id: RegisterNewUserId): Promise<Nullable<RegisterNewUser>> {
    return new Promise<Nullable<RegisterNewUser>>((resolve, reject) => {
      return null;
    });
  }
}
