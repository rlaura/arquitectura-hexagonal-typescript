import { IEventBus } from "../../../../../../core/domain/bus/event/IEventBus";
import { RegisterNewUser } from "../../../domain/entities/RegisterNewUser";
import { IRegisterNewUserRepository } from "../../../domain/repositories/IRegisterNewUserRepository";
import { EmailAddress } from "../../../domain/value-objects/EmailAddress";
import { Password } from "../../../domain/value-objects/Password";
import { RegisterNewUserId } from "../../../domain/value-objects/RegisterNewUserId";
import { UserName } from "../../../domain/value-objects/UserName";
import { RegisterNewUserCommand } from "./RegisterNewUserCommand";

export class RegisterNewUserCreator {
  private _repository: IRegisterNewUserRepository;
  // private _eventBus: IEventBus;

  constructor(
    repository: IRegisterNewUserRepository
    // , eventBus: IEventBus
  ) {
    this._repository = repository;
    // this._eventBus = eventBus;
  }

  async run(params: {
    id: RegisterNewUserId;
    emailAddress: EmailAddress;
    userName: UserName;
    password: Password;
  }): Promise<void> {
    const registrationDate = new Date();
    const registrerNewUser = RegisterNewUser.UserRegistration(
      params.id,
      params.emailAddress,
      params.userName,
      params.password,
      registrationDate
    );

    await this._repository.save(registrerNewUser);
    // await this._eventBus.publish(registrerNewUser.pullDomainEvents());
  }
}
