import { ICommand } from "../../../../../../core/domain/bus/command/ICommand";
import { ICommandHandler } from "../../../../../../core/domain/bus/command/ICommandHandler";
import { Uuid } from "../../../../../../core/domain/value-objects/Uuid";
import { EmailAddress } from "../../../domain/value-objects/EmailAddress";
import { Password } from "../../../domain/value-objects/Password";
import { RegisterNewUserId } from "../../../domain/value-objects/RegisterNewUserId";
import { UserName } from "../../../domain/value-objects/UserName";
import { RegisterNewUserCommand } from "./RegisterNewUserCommand";
import { RegisterNewUserCreator } from "./RegisterNewUserCreator";


export class RegisterNewUserCommandHandler implements ICommandHandler<RegisterNewUserCommand> {
  constructor(private _registerNewUserCreator: RegisterNewUserCreator) {}

  subscribedTo(): ICommand {
      return RegisterNewUserCommand;
  }

  async handle(command: RegisterNewUserCommand): Promise<void> {
      const id = new RegisterNewUserId(Uuid.generate().toString());
      const emailAddress = new EmailAddress(command.emailAddress);
      const userName = new UserName(command.userName);
      const password = new Password(command.password);

      await this._registerNewUserCreator.run({id,emailAddress,userName,password});
  }
}