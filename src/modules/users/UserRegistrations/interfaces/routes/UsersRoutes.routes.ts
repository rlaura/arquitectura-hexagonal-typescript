import { Request, Response, Router } from "express";
import { ICommandBus } from "../../../../../core/domain/bus/command/ICommandBus";
import { RegisterNewUserController } from "../controllers/RegisterNewUserController";
import { RegisterNewUserCommand } from "../../application/use-case/registerNewUser/RegisterNewUserCommand";
import { RegisterNewUserCommandHandler } from "../../application/use-case/registerNewUser/RegisterNewUserCommandHandler";
import { RegisterNewUserCreator } from "../../application/use-case/registerNewUser/RegisterNewUserCreator";
import { InMemoryCommandBus } from "../../../../../core/infrastructure/bus/command/InMemoryCommandBus";
import { CommandHandlers } from "../../../../../core/infrastructure/bus/command/CommandHandlers";
import { PrismaRegisterNewUser } from "../../infrastructure/persistence/repositories/PrismaRegisterNewUser";

const UsersRoutes = Router();
//https://github.com/SomeBlackMagic/alert-mapper
//https://github.com/SomeBlackMagic/prometheus-alerting-middleware/blob/597d6d1f41400c3a12b89c3953e22422a24a05e3/src/Kernel/Tactician/CommandBus.ts
//https://github.com/MilanDouglas/bitloops-language/blob/29e9e94b2d5e368e885b101a4b97a745e088f5f0/boilerplate/ts/core/src/Container.ts#L65


//https://github.com/Giaeulate/dazl-back/blob/4d19b5b29d8ba6673597f82a24e45faca8d530d2/src/Contexts/Dazl/user-live/application/desactive/UserLiveDesactive.ts#L40
//https://github.com/coal182/typescript-ddd-shop/blob/ee8b91ad171f7952c4b2c1fa741bafa542585d0e/src/contexts/shop/cart/application/command-handlers/add-item-to-cart-command-handler.ts#L30
//https://github.com/coal182/typescript-ddd-shop/tree/ee8b91ad171f7952c4b2c1fa741bafa542585d0e/src/apps
//https://github.com/simnova/ownercommunity/blob/2dd150f6d477be03b33339070bf361688af3842d/data-access/seedwork/event-bus-seedwork-node/node-event-bus.ts

//https://github.com/EusebiuAndrei/mem-framework/blob/1172e4a47e9628adb65e0f18785dca5121fc49de/src/mem-ddd/examples/meetingProposal/MeetingProposal.ts

// https://github.com/SomeBlackMagic/prometheus-alerting-middleware/blob/597d6d1f41400c3a12b89c3953e22422a24a05e3/src/Kernel/Tactician/CommandBus.ts#L21
// https://github.com/SomeBlackMagic/alert-mapper/blob/8f223430215fd5fc00ca0c28edcdd01110e84b46/src/Core/Tactician/CommandBus.ts#L20
// https://github.com/gonzalogcontacto/ddd-crud-express-typescript/blob/3a4335c4e568b91ba1cce92ffb39594f70c8a9eb/apps/api/config/commandBus.ts#L17


UsersRoutes.post("/user", async (req: Request, res: Response) => {
  // const courseRepository = new PrismaRegisterNewUser();
  // const couseCreator = new RegisterNewUserCreator(courseRepository);
  // const comandHandler = new RegisterNewUserCommandHandler(couseCreator);
  // const couseComandHandler = new CommandHandlers([comandHandler]);
  // const comandbus = new InMemoryCommandBus(couseComandHandler);
  // const userGetController = new RegisterNewUserController(comandbus);

  const userGetController = 
        new RegisterNewUserController(
          new InMemoryCommandBus(
            new CommandHandlers([
              new RegisterNewUserCommandHandler(
                new RegisterNewUserCreator(new PrismaRegisterNewUser())
              ),
            ])
          )
        );

  return userGetController.run(req, res);
});

export default UsersRoutes;
// https://paulallies.medium.com/clean-architecture-typescript-express-api-b90846794998
