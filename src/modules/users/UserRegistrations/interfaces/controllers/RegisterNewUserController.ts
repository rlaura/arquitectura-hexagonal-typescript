import { Request, Response } from "express";
import httpStatus from "http-status";

import { ICommandBus } from "../../../../../core/domain/bus/command/ICommandBus";
import { IController } from "../../../../../core/interfaces/IController";
import { RegisterNewUserCommand } from "../../application/use-case/registerNewUser/RegisterNewUserCommand";

export class RegisterNewUserController implements IController {
  constructor(private readonly commandBus: ICommandBus) {}

  async run(req: Request, res: Response) {
    try {
      const { email, userName, password } = req.body;
      
       // Crear un objeto de tipo ICommandBus con los datos recibidos
      const createCourseCommand = new RegisterNewUserCommand(
        email,
        userName,
        password
      );

      // Llamar al método dispatch del commandBus y pasarle el comando creado
      await this.commandBus.dispatch(createCourseCommand);

      res.status(httpStatus.CREATED).send();
    } catch (error) {
      res.status(httpStatus.INTERNAL_SERVER_ERROR).send();
    }
  }
}
