import { IBusinessRule } from "../../../../../core/domain/value-objects/IBusinessRule";
import { IUsersCounter } from "../value-objects/IUsersCounter";

/**
 * Regla de negocio que verifica si el login de usuario es único.
 */
export class UserLoginMustBeUniqueRule implements IBusinessRule {
  private readonly _usersCounter: IUsersCounter;
  private readonly _login: string;

  /**
   * Crea una nueva instancia de UserLoginMustBeUniqueRule.
   * @param {IUsersCounter} usersCounter - Objeto para contar usuarios.
   * @param {string} login - El login del usuario.
   * @return {UserLoginMustBeUniqueRule} Una nueva instancia de UserLoginMustBeUniqueRule.
   */
  constructor(usersCounter: IUsersCounter, login: string) {
      this._usersCounter = usersCounter;
      this._login = login;
  }

  /**
   * Verifica si la regla de negocio está rota.
   * @return {boolean} Devuelve true si el login de usuario no es único, de lo contrario devuelve false.
   */
  public isBroken(): boolean {
      return this._usersCounter.CountUsersWithLogin(this._login) > 0;
  }

  /**
   * El mensaje descriptivo de la regla de negocio.
   * @return {string} El mensaje descriptivo de la regla de negocio.
   */
  public get message(): string {
      return "El inicio de sesión del usuario debe ser único";
  }
}