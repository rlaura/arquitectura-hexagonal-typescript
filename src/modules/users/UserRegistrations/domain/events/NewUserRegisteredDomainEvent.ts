import { DomainEvent } from "../../../../../core/domain/bus/event/DomainEvent";

type NewUserRegisteredDomainEventAttributes = {
  readonly duration: string;
  readonly name: string;
};

export class NewUserRegisteredDomainEvent extends DomainEvent {
  static readonly EVENT_NAME = 'newUser.Registered';

  readonly duration: string;
  readonly name: string;

  constructor({
    aggregateId,
    name,
    duration,
    eventId,
    occurredOn
  }: {
    aggregateId: string;
    eventId?: string;
    duration: string;
    name: string;
    occurredOn?: Date;
  }) {
    super(NewUserRegisteredDomainEvent.EVENT_NAME, aggregateId, eventId, occurredOn);
    this.duration = duration;
    this.name = name;
  }

  toPrimitives(): NewUserRegisteredDomainEventAttributes {
    const { name, duration, aggregateId } = this;
    return {
      name,
      duration,
      // eventName: NewUserRegisteredDomainEvent.EVENT_NAME,
      // id: aggregateId
    };
  }

  static fromPrimitives(params: {
    aggregateId: string;
    attributes: NewUserRegisteredDomainEventAttributes;
    eventId: string;
    occurredOn: Date;
  }): DomainEvent {
    const { aggregateId, attributes, occurredOn, eventId } = params;
    return new NewUserRegisteredDomainEvent({
      aggregateId,
      duration: attributes.duration,
      name: attributes.name,
      eventId,
      occurredOn
    });
  }
}
