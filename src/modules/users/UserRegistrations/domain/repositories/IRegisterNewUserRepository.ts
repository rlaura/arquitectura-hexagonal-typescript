import { Nullable } from "../../../../../core/domain/value-objects/Nullable";
import { RegisterNewUser } from "../entities/RegisterNewUser";
import { RegisterNewUserId } from "../value-objects/RegisterNewUserId";

export interface IRegisterNewUserRepository {
  save(course: RegisterNewUser): Promise<void>;
  search(id: RegisterNewUserId): Promise<Nullable<RegisterNewUser>>;
}
