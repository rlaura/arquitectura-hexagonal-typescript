export class UserRegistrationStatus {
  static WaitingForConfirmation: UserRegistrationStatus = new UserRegistrationStatus('WaitingForConfirmation');
  static Confirmed: UserRegistrationStatus = new UserRegistrationStatus('Confirmed');
  static Expired: UserRegistrationStatus = new UserRegistrationStatus('Expired');

  readonly value: string;

  private constructor(value: string) {
      this.value = value;
  }
}