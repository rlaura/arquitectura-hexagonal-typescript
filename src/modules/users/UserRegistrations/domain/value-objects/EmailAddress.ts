export class EmailAddress {
  private readonly _value: string;

  constructor(value: string) {
    // Validación básica del formato del correo electrónico
    if (!this.isValidEmail(value)) {
      throw new Error("Invalid email address");
    }
    this._value = value;
  }

  get value(): string {
    return this._value;
  }

  private isValidEmail(email: string): boolean {
    // Validación del formato del correo electrónico (simplificado)
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
  }
}
