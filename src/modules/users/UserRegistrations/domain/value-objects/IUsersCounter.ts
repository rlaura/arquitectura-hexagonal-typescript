/**
 * Define un contador de usuarios.
 */
export interface IUsersCounter {
  /**
   * Cuenta la cantidad de usuarios con un login específico.
   * @param {string} login - El login del usuario a contar.
   * @return {number} El número de usuarios con el login especificado.
   */
  CountUsersWithLogin(login: string): number;
}
