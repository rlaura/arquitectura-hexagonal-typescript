import { AggregateRoot } from "../../../../../core/domain/Aggregate/AggregateRoot";
import { Uuid } from "../../../../../core/domain/value-objects/Uuid";
import { EmailAddress } from "../value-objects/EmailAddress";
import { Password } from "../value-objects/Password";
import { RegisterNewUserId } from "../value-objects/RegisterNewUserId";
import { UserName } from "../value-objects/UserName";
import { UserRegistrationStatus } from "../value-objects/UserRegistrationStatus";

export class RegisterNewUser extends AggregateRoot {
  readonly _id: RegisterNewUserId;
  readonly _emailAddress: EmailAddress;
  readonly _userName: UserName;
  readonly _password: Password;
  readonly _registrationDate: Date;
  // readonly _registrationStatus: UserRegistrationStatus;
  // readonly _registrationConfirmationDate?: Date;

  constructor(
    id: RegisterNewUserId,
    emailAddress: EmailAddress,
    userName: UserName,
    password: Password,
    registrationDate: Date
    // registrationStatus: UserRegistrationStatus,
    // registrationConfirmationDate?: Date
  ) {
    super();

    // this._id = new RegisterNewUserId(Uuid.generate().toString());
    this._id = id;
    this._emailAddress = emailAddress;
    this._userName = userName;
    this._password = password;
    this._registrationDate = registrationDate;
    // this._registrationStatus = registrationStatus;
    // this._registrationConfirmationDate = registrationConfirmationDate;

    // this.addDomainEvent();
  }

  static UserRegistration(
    id: RegisterNewUserId,
    emailAddress: EmailAddress,
    userName: UserName,
    password: Password,
    registrationDate: Date
    // registrationStatus: UserRegistrationStatus,
    // registrationConfirmationDate?: Date
  ): RegisterNewUser {
    return new RegisterNewUser(
      id,
      emailAddress,
      userName,
      password,
      registrationDate
      // registrationStatus
    );
  }
  /**
   * @override
   */
  toPrimitives() {
    throw new Error("Method not implemented.");
  }
}
