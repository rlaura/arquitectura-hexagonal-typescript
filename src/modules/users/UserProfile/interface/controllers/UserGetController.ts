import { Request, Response } from "express";
import { IController } from "../../../../../core/interfaces/IController";
import { IQueryBus } from "../../../../../core/domain/bus/query/IQueryBus";


export class UserGetController implements IController {
  constructor(private queryBus: IQueryBus) {}

  async run(req: Request, res: Response): Promise<void> {
    try {
      res.send("query");
    } catch (error) { 
      res.send("Hello, world!");
    }
  }
}
