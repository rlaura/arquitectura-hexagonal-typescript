
import * as fs from "fs";
import { IServer } from "./apps/rest-api/IServer";
import { ServerHttp } from "./apps/rest-api/ServerHttp";
import { ServerHttps } from "./apps/rest-api/ServerHttps";


export class AppServer {
  server?: IServer;

  async start() {
    const port = process.env.PORT || "3000";
    const currentDate: Date = new Date();
    // const currentUtcDate: Date = new Date(currentDate.toISOString());
    console.log(currentDate);
    // console.log(currentUtcDate);
    if (process.env.NODE_ENV === "development") {
      this.server = new ServerHttp(port);
      return this.server.listen();
    }

    if (process.env.NODE_ENV === "production") {
      //TODO: configurar con los archivos de los certificados
      const httpsOptions = {
        key: fs.readFileSync("ruta/al/archivo/key.pem"),
        cert: fs.readFileSync("ruta/al/archivo/cert.pem"),
      };
      this.server = new ServerHttps(port, httpsOptions);
      return this.server.listen();
    }

    this.server = new ServerHttp(port);
    return this.server.listen();
  }

  async stop() {
    return this.server?.stop();
  }
}
