# Proyecto node typescript con Domain-Driven Design (DDD)

## Creando un proyecto node
### Iniciación de un Proyecto Node.js

Para iniciar un proyecto Node.js, sigue estos pasos:

1. Abre tu terminal.
2. Navega hasta la carpeta donde quieres crear tu proyecto.
3. Ejecuta el siguiente comando para inicializar un proyecto Node.js:

```bash
npm init -y
```
### Instalación e Iniciación de un Proyecto TypeScript

1. Instala TypeScript como una dependencia de desarrollo ejecutando el siguiente comando:
```bash
npm i typescript -D
```

2. Crea un archivo `tsconfig.json` en la raíz de tu proyecto para configurar TypeScript. Puedes hacerlo ejecutando:
```bash
npx tsc --init
```
### Instalación de ts-node y nodemon

1. Instala `ts-node` ejecutando el siguiente comando:
```bash
npm i -D ts-node
```

2. Instala `nodemon` ejecutando el siguiente comando:
```bash
npm i nodemon -D
```

### Instalación de Express y dotenv
1. Instala `Express` y `dotenv` ejecutando el siguiente comando:
```bash
npm i express dotenv
npm install -D typescript @types/node @types/express
```